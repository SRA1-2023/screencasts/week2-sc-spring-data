package dao;


import org.springframework.data.repository.Repository;

import flights.Airline;

public interface AirlineRepository extends Repository<Airline, Integer> {

	// TODO : find airline by ID
	
	// TODO : find all in a page
	
	// TODO : find all by country
	
	// TODO : find all existing countries	
	
}
